//
//  FirstViewController.swift
//  tabapp
//
//  Created by bouh on 27/01/2020.
//  Copyright © 2020 bouh. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
//    struct Serverresult: Codable {
//        var date: String
//        var result1: String
//        var result2: String
//        var result3: String
//        var result4: String
//        var result5: String
//    }
    
    struct ServerResult: Codable {
        var date: String
        var result1: String
        var result2: String
        var result3: String
        var result4: String
        var result5: String
    }
    
    var datehistorique : String = ""
    var numeroimage : String = ""
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titrehome: UILabel!
    @IBAction func accesphoto(_ sender: Any) {
        print("Vous avez appuyez sur start")
//        var i = imageView.
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        
        let actionSheet = UIAlertController(title: "Photo", message: "Choisir la source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Appareil photo", style: .default, handler: {(action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                print("Camera pas disponible")
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Galerie", style: .default, handler: {(action:UIAlertAction) in imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        imageView.image = image
        picker.dismiss(animated: true, completion: nil)
        let imageData = image.pngData()!
        let imageStr = imageData.base64EncodedString(options: .endLineWithCarriageReturn)
        //print("Voici imagedata dans le start : ", imageData)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
   
    var res : ServerResult? = nil
    var jsonresult : Data? = nil
    var product : ServerResult? = nil //test
    
 //-------------------------------------test---------------------------------------------------
    
    let json1 = """
    {
        "date": "12344",
        "result1": "blabla",
        "result2": "blabla",
        "result3": "blabla",
        "result4": "blabla",
        "result5": "blabla",
    }
    """.data(using: .utf8)!
    
    @IBAction func sendpost(_ sender: Any) {
        
        let decoder = JSONDecoder()
        self.product = try? decoder.decode(ServerResult.self, from: json1)
        
        
        //variables
        var status = 0
        //var locationimage = "nil"
        
        
        //let image = imageView.image?.resizableImage(withCapInsets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8), resizingMode: .stretch)
        let image = imageView.image?.resizableImage(withCapInsets: UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8), resizingMode: .stretch).pngData()!
        
        let imageStr = image?.base64EncodedString()
 //       print("Voici imagedata dans le send:", image ?? nil)
        
        //print(image)
        
        if image != nil {
            let session = URLSession.shared
            let url = URL(string: "http://25.127.98.73:8000/img_search/")!

            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let client = UIDevice.current.name
            let json = [
                "image": String(imageStr!),
                "client":client
            ]
                
        
            
            let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
            //let jsonDatastring = String(data: jsonData, encoding: String.Encoding.utf8) ?? "Data could not be printed"
            print("voici le jsondata:",jsonData)
            
         //--------------------------------SENDPOST---------------------------------------------------------------
            let task = session.uploadTask(with: request, from: jsonData) { data, response, error in
                //if let data = data, let dataString = String(data: data, encoding: .utf8) {
                    //print("Ici la reponses : ")
                    //print(dataString)
                    //locationimage = dataString
                //}
                if let error = error {
                    print("error: \(error)")
                } else {
                    if let response = response as? HTTPURLResponse {
                        print("statusCode: \(response.statusCode)")
                        //status = response.statusCode
                        
                    }
                    if let data = data, let dataString = String(data: data, encoding: .utf8) {
                        print("data: \(dataString)")
                        let locationimage = dataString
                        self.numeroimage = locationimage
                        //let locationimage = "/img_search/8"
                        //--------------------------------SENDGET--------------------------------
                        
                        //let url = URL(string: "https://jsonplaceholder.typicode.com"+locationimage)!
                        let url = URL(string: "http://25.127.98.73:8000/img_search/"+locationimage)!
                        print(url)
                        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                            if let error = error {
                                print("error: \(error)")
                            } else {
                                if let response = response as? HTTPURLResponse {
                                    print("statusCode: \(response.statusCode)")
                                }
                                if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                    print("data: \(dataString)")
                                    self.jsonresult = dataString.data(using: .utf8)!
                                    let decoder = JSONDecoder()
                                    self.res = try! decoder.decode(ServerResult.self, from: self.jsonresult!)
                                    self.datehistorique = self.res!.date
                                }
                            }
                        }
                        task.resume()
                        
                    }
                }
            }

            task.resume()
            
            
        } else {
            print("Vous n'avez pas selectionner d'image")
        }
        
        
    }
   
    @IBAction func voirresult(_ sender: Any) {
        
        //-------------------------------------test----------------------------------------------------

        //let resulatp = product
        //print(resulatp)
        
        performSegue(withIdentifier: "name", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var vc = segue.destination as! Resultats_Scene
        vc.finalresult = self.res
        vc.numeroimage = numeroimage
        vc.datehistorique = datehistorique
    }
    
    
}


