from django.apps import AppConfig


class ImagemoAppConfig(AppConfig):
    name = 'ImageMo_App'
