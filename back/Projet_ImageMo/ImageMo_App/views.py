from django.shortcuts import render
from django.contrib.auth.models import User, Group
from django.utils.datetime_safe import datetime
from rest_framework import viewsets
from rest_framework.utils import json
from django.core.serializers.json import DjangoJSONEncoder

from ImageMo_App.models import ImgSearchResult as ImgSearchResultModel
from ImageMo_App.models import Querys as QuerysModel
from ImageMo_App.models import ImgSearchQuery as ImgSearchQueryModel
from ImageMo_App.serializers import UserSerializer, GroupSerializer, ImgSearchQuerySerializer, QuerysSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from Projet_ImageMo.query_online import decodeBase64, codeBase64
from Projet_ImageMo.query_online import search
import django


class ImgSearchQuery(APIView):

    '''
    def get_object(self, pk):
        try:
            imgSearchQuery = ImgSearchQuery.objects.get(pk=pk)
        except ImgSearchQuery.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
    '''

    def post(self, request, *arg, **kwargs):
        serializer = ImgSearchQuerySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            searchQuery = ImgSearchQueryModel(image=request.data['image'], date=datetime.now(),
                                              client=request.data['client'])
            searchQuery.save()
            decodeBase64(serializer.data['image'])
            searchans = search()
            query = QuerysModel(queryPath=searchans[0], result1=searchans[1], result2=searchans[2],
                                result3=searchans[3],
                                result4=searchans[4], result5=searchans[5])
            query.save()
            result = ImgSearchResultModel(query=searchQuery, result=query)  # query=request,
            result.save()
            return Response(result.pk, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        print('result_id :')
        pk = self.kwargs.get('pk')
        print(pk)
        result_id = 6
        try:
            searchResult = ImgSearchResultModel.objects.get(pk=pk)
        except ImgSearchResultModel.objects.get(pk=pk).DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        searchResult = ImgSearchResultModel.objects.get(pk=pk)
        res = searchResult.result
        json_data = {"date": searchResult.query.date, "result1": codeBase64(res.result1).decode("utf-8"),
                     "result2": codeBase64(res.result2).decode("utf-8"),
                     "result3": codeBase64(res.result3).decode("utf-8"),
                     "result4": codeBase64(res.result4).decode("utf-8"),
                     "result5": codeBase64(res.result5).decode("utf-8")}
        print(codeBase64(res.result1).decode("utf-8"))
        json_dump = json.dumps(json_data, sort_keys=True,  indent=1, cls=DjangoJSONEncoder)
        print(json_dump)
        json_object = json.loads(json_dump)
        return Response(json_object)
        #return Response(searchResult.query.date, codeBase64(res.result1), codeBase64(res.result2),
         #               codeBase64(res.result3), codeBase64(res.result4), codeBase64(res.result5))

'''
class Query(APIView):
    def get_object(self, pk):
        try:
            query = QuerysModel.objects.get(pk=pk)
        except QuerysModel.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
'''

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
