from django.contrib.auth.models import User, Group
from .models import ImgSearchQuery
from .models import Querys
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class ImgSearchQuerySerializer(serializers.ModelSerializer):
    class Meta:
        model = ImgSearchQuery
        fields = ['image', 'date', 'client']

class QuerysSerializer(serializers.ModelSerializer):
    class Meta:
        model = Querys
        fields = ['querryPath', 'result1', 'result2', 'result3', 'result4', 'result5']

