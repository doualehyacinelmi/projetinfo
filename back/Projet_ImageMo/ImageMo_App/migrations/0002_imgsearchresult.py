# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2020-02-04 15:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ImageMo_App', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImgSearchResult',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.DateTimeField(max_length=255)),
            ],
        ),
    ]
