from django.db import models
from Projet_ImageMo.extract_cnn_vgg16_keras import VGGNet

import numpy as np
import h5py

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import base64
import time
import os
import cv2
import random


def decodeBase64(string64): #should take as parameter data['img'] from json or could take json directly

    biteArray = string64.encode()  #encode string to byteArray
    path = "search_img"+ time.strftime(r"%Y-%m-%d_%H-%M-%S", time.localtime()) + ".jpg"        #data['format'].lower()-> to get format from json   #Save in directory
    #decode base64 to image
    with open(path, "wb") as fh:
        fh.write(base64.decodebytes(biteArray))
    return path

def codeBase64(binary_img_path):
    with open(binary_img_path, "rb") as image_file:
        string64 = base64.b64encode(image_file.read())
    return string64

def search():
    # read in indexed images' feature vectors and corresponding image names
    #h5f = h5py.File(args["index"], 'r')
    h5f = h5py.File('./featureCNN.h5', 'r')
    feats = h5f['dataset_feat'][:]
    imgNames = h5f['dataset_name'][:]
    h5f.close()

    print("--------------------------------------------------")
    print("               searching starts")
    print("--------------------------------------------------")

    # read and show query image
    #queryDir = args["query"]
    #queryDir = './database/001_accordion_image_0001.jpg'
    files = []
    for myfile in os.listdir("./"):
        if ".jpg" in myfile:
            files.append(myfile)
    files.sort(reverse=True)

    queryDir = './' + files[0]

    print(queryDir)

    queryImg = mpimg.imread(queryDir)
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.imshow(queryImg)
    plt.title("Query Image")
    plt.axis('off')

    # init VGGNet16 model
    model = VGGNet()

    # extract query image's feature, compute simlarity score and sort
    queryVec = model.extract_feat(queryDir)
    scores = np.dot(queryVec, feats.T)
    rank_ID = np.argsort(scores)[::-1]
    rank_score = scores[rank_ID]
    # print rank_ID
    # print rank_score


    # number of top retrieved images to show
    maxres = 5
    imlist = [imgNames[index] for i,index in enumerate(rank_ID[0:maxres])]
    print("top %d images in order are: " %maxres, imlist)
    #'''
    a=0
    for binary_path in imlist:
        imlist[a] = binary_path.decode("utf-8")
        a = a+1
    #'''

    # show top #maxres retrieved result one by one
    a = 0
    for i in imlist:

        image = cv2.imread(imlist[a])   #.decode("utf-8"))
        pt = (0, 3 * image.shape[0] // 4)
        search_id = "search output %d" % (a + 1)
        #cv2.putText(image, search_id, pt, cv2.FONT_HERSHEY_SCRIPT_COMPLEX, 2, [0, 255, 0], 2)
        cv2.namedWindow(search_id, cv2.WINDOW_NORMAL)
        cv2.imshow(search_id, image)
        k = 0
        a += 1
        while (k != 32):
            k = cv2.waitKey(0)
            if (k == 27):
                exit(0)
        cv2.destroyAllWindows()


    return queryDir, imlist[0], imlist[1], imlist[2], imlist[3], imlist[4]
